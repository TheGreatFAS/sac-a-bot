/*
Projet: Sac-à-Bot
Equipe: P-01
Auteurs: Les membres auteurs du script
Description: Breve description du script
Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */

#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include <math.h>     //Importe les fonctions de mathématique
#include <PID.h>    //Importe les fonctions du programme PID.c
#include <fonctions_moteurs.h> //Importe les fonctions des moteurs (tourner, avancer ligne droite, accel...)

/* ****************************************************************************
Variables globales et defines
**************************************************************************** */
// -> defines...
// L'ensemble des fonctions y ont acces

/* ****************************************************************************
Vos propres fonctions sont creees ici
**************************************************************************** */

/* ****************************************************************************
Fonctions d'initialisation (setup)
**************************************************************************** */
// -> Se fait appeler au debut du programme
// -> Se fait appeler seulement une fois
// -> Generalement on y initialise les variables globales

void setup()
{
  if(ROBUS_IsBumper(3)){
    BoardInit();
  }
}

/* ****************************************************************************
Fonctions de boucle infini (loop())
**************************************************************************** */
// -> Se fait appeler perpetuellement suite au "setup"

void loop()
{
  //Initialisation des différentes variables
  int i = 0;
  float ajustement_largeur_robot = (45 - DISTANCE_ROUES_28)/2;
  int angles[6];
  float segments[7];

  //Array des angles dans le parcours
  //Un angle négatif représente un virage à gauche
  //Un angle positif représente un virage à droite
  angles[0] = -90;
  angles[1] = 90;
  angles[2] = 90;
  angles[3] = -90;
  angles[4] = 90;
  angles[5] = -90;

  //Array des segments de ligne droite dans le parcours
  //Un ajustement doit être fait pour contrer la largeur du parcours lorsque le robot arrive à une intersection
  segments[0] = 205 + 0.5 * ajustement_largeur_robot;
  segments[1] = 100 - 2 * ajustement_largeur_robot;
  segments[2] = 48 - 2 * ajustement_largeur_robot;
  segments[3] = 54 - 1 * ajustement_largeur_robot;
  segments[4] = 105 - 2 * ajustement_largeur_robot;
  segments[5] = 45 - 1.5 * ajustement_largeur_robot;
  segments[6] = 105 + ajustement_largeur_robot;

  //Boucle for qui passe chacun des éléments dans l'array des segments
  //La boucle appel la fonction ligne_droite pour le segment et la fonction tourner_gauche si l'angle est négatif
  //ou tourner_gauche si l'angle est positif
  for (i = 0; i <= 7; i = i + 1){

    delay(100);
    Serial.print("Segment ");  
    Serial.print(i); 
    Serial.print(" de ");
    Serial.println(segments[i]);
    ligne_droite(segments[i]); 

    if(i < sizeof(segments)){
      if(angles[i] < 0){
        //Envoie la valeur absolue de l'angle négatif
        tourner_gauche(abs(angles[i]));
      }
      else{
        tourner_droite(angles[i]);
      }
    }
  }
  Serial.println("Début du demi-tour");
  demi_tour();

  for (i = 7; i == 0; i = i - 1){
    ligne_droite(segments[i]);
    Serial.print("Segment ");  
    Serial.print(i); 
    Serial.print(" de ");
    Serial.print(segments[i]); 
    if(angles[i] < 0){
      //Envoie la valeur absolue de l'angle négatif
      tourner_gauche(abs(angles[i]));
    }
    else{
      tourner_droite(angles[i]);
    }
  }
  // SOFT_TIMER_Update(); // A decommenter pour utiliser des compteurs logiciels */
  delay(5000); // Delais pour décharger le CPU
}