/**
* \file   fonctions_moteurs.cpp
* \author Team Sac-A-Bot
* \date   22 octobre 2019
* \brief  Librairie des fonctions permettant de gérer les moteurs
* \version 1.0
*/

#include "fonctions_moteurs.h"

const double CIRCONFERENCE_ROUE_28 = M_PI * 7.55;
const double CIRCONFERENCE_ROUE_GAUCHE = M_PI * 7.55;
const double CIRCONFERENCE_ROUE_GAUCHE_02 = M_PI * 6.8;
const double CIRCONFERENCE_ROUE_02 = M_PI * 7.3;

void ligne_droite(int longueur)
{

  reset_encoders();
  float nbr_tour_roue = longueur / CIRCONFERENCE_ROUE_28;
  int pulse_cible = round(nbr_tour_roue * 3200);
  int pulse_parcourue = 0;
  acceleration(pulse_parcourue);

  while (pulse_parcourue < pulse_cible)
  {
    pulse_parcourue = pulse_parcourue + ENCODER_Read(0);
    PID(0.4);
    delay(50);
  }
  deceleration();
  reset_encoders();
}

void reset_encoders()
{
  ENCODER_ReadReset(MOTEUR_GAUCHE);
  ENCODER_ReadReset(MOTEUR_DROITE);
}

void tourner_gauche(int degre)
{
  double nbr_tour_roue = ((2 * M_PI * DISTANCE_ROUES_28) / (360 / degre)) / CIRCONFERENCE_ROUE_28;
  int nbr_pulse_necessaire = nbr_tour_roue * 3200;

  MOTOR_SetSpeed(MOTEUR_GAUCHE, 0);
  MOTOR_SetSpeed(MOTEUR_DROITE, 0);
  reset_encoders();

  delay(100);
  MOTOR_SetSpeed(MOTEUR_DROITE, VITESSE_VIRAGE);

  while (ENCODER_Read(1) < nbr_pulse_necessaire)
  {
    //Attends...
  }
  MOTOR_SetSpeed(MOTEUR_DROITE, 0);
  reset_encoders();
}

void tourner_droite(int degre)
{
  MOTOR_SetSpeed(0, 0);
  MOTOR_SetSpeed(1, 0);
  reset_encoders();
  double nbr_tour_roue = ((2 * M_PI * DISTANCE_ROUES_28) / (360 / degre)) / CIRCONFERENCE_ROUE_GAUCHE;
  int nbr_pulse_necessaire = nbr_tour_roue * 3200;
  delay(100);
  MOTOR_SetSpeed(0, VITESSE_VIRAGE);

  while (ENCODER_Read(0) < nbr_pulse_necessaire)
    ;
  {
  }
  MOTOR_SetSpeed(0, 0);
  reset_encoders();
}

void demi_tour()
{
  MOTOR_SetSpeed(1, 0);
  MOTOR_SetSpeed(0, 0);
  reset_encoders();

  //déterminer combien de tour d'encodeur nécessaire pour 180 degrés
  double rayon = DISTANCE_ROUES_02 / 2;
  //moitié de la circonférance

  double distance = (M_PI * rayon) / 2 + 10;
  double distance_parcourue0 = 0;

  MOTOR_SetSpeed(0, -VITESSE_VIRAGE);
  MOTOR_SetSpeed(1, VITESSE_VIRAGE);

  while (distance_parcourue0 < distance)
  {
    distance_parcourue0 = (CIRCONFERENCE_ROUE_GAUCHE) * (ENCODER_Read(1) / 3200);
  }
  MOTOR_SetSpeed(0, 0);
  MOTOR_SetSpeed(1, 0);
  reset_encoders();
}

void acceleration(int pulse_parcourue)
{
  for (int i = 1; i < 40; i++)
  {
    MOTOR_SetSpeed(MOTEUR_GAUCHE, (double)i / 100);
    MOTOR_SetSpeed(MOTEUR_DROITE, (double)i / 100);
    PID((double)i / 100);
    pulse_parcourue = pulse_parcourue + ENCODER_Read(0);
    delay(25);
  }
}

void deceleration()
{
  for (int i = 40; i > 0; i--)
  {
    MOTOR_SetSpeed(MOTEUR_GAUCHE, (double)i / 100);
    MOTOR_SetSpeed(MOTEUR_DROITE, (double)i / 100);
    PID((double)i / 100);
    delay(25);
  }
}