#include "PID.h" 
#include <LibRobus.h>
 int nbPulseParCycle0 = 0;    //Valeur statique
 int nbPulseParCycle1 = 0;    //Valeur statique
 double vitesseMoteur = 0;   //Valeur statique
 

void PID(double valMoteurInitiale)
{
    
  
    float erreurP = 0;
    float erreurI = 0;
    float correction = 0;
    double Kp = KpRobot1;
    double Ki = KiRobot1;
    

    nbPulseParCycle0 = nbPulseParCycle0 + ENCODER_Read(0);
    nbPulseParCycle1 = nbPulseParCycle1 + ENCODER_Read(1);


    erreurP = 0.03000000+(nbPulseParCycle0 - nbPulseParCycle1) * Kp;
    erreurI = (ENCODER_Read(0) - ENCODER_Read(1)) * Ki;
    correction = erreurP + erreurI;
    
    vitesseMoteur = valMoteurInitiale + correction;
    
    MOTOR_SetSpeed(1, vitesseMoteur);
    MOTOR_SetSpeed(0, valMoteurInitiale);
    //Serial.print("Encodeur master:");
    //Serial.println(ENCODER_Read(0));
    //Serial.print("Encodeur slave:");
    //Serial.println(ENCODER_Read(1));
    ENCODER_ReadReset(0);
    ENCODER_ReadReset(1);
}